package domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;



@Service
public class CarroService {
	
	public List<Carro> getCarros(){
		List<Carro> carros = new ArrayList<>();
		
		carros.add(new Carro (1L, "Fusca"));
		carros.add(new Carro (1L, "Brasilia"));
		carros.add(new Carro (1L, "Del Rey"));
		carros.add(new Carro (1L, "Chevette"));
		carros.add(new Carro (1L, "Opala"));
		
		return carros;
	}
}
